<?php

namespace BlogIntegration;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

// use Blog\Domain\Email;
use Blog\Domain\User;
use Blog\Aplication\Command\CreateUserCommand;
use Blog\Aplication\CommandHandler\CreateUserCommandHandler;
use Blog\Infraestructure\User\MemoryUserRepository;
use Blog\Infraestructure\User\Exception;
use PHPUnit_Framework_Assert;

/**
 * Defines application features from the specific context.
 */
class CreateUserContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    private $user;
    private $email;
    private $password;
    private $emailDto;
    private $passwordDto;
    private $response;

    private $memoryUserRepository;
    private $createUserCommandHandler;
    // Given email 'dan@dan.dan'
    // When creating new User
    public function __construct()
    {
        $this->memoryUserRepository = new MemoryUserRepository();
        //     $this->createUserCommandHandler = new CreateUserCommandHandler($this->memoryUserRepository);

    //     $this->password = $password;
    }

    /**
     * @Given email :email
     */
    public function givenEmail($email)
    {
        $this->email = $email;
        // $this->emailDto = new Email($this->email);
    }

    /**
    * @Given password :password
    */
    public function givenPassword($password)
    {
        $this->password = $password;
    }

    /**
    * @When creating new User
    */
    public function creatingNewUser()
    {
        $createUserCommand = new CreateUserCommand($this->email, $this->password);
        $createUserCommandHandler = new CreateUserCommandHandler($this->memoryUserRepository);
        $this->response = $createUserCommandHandler->handle($createUserCommand);
    }

    /**
     * @Then I see correct welcome message
     */
    public function IseeCorrectWelcomeMessage()
    {
        PHPUnit_Framework_Assert::assertTrue('User created with email: '.$this->email == $this->response);
    }

    /**
     * @Then I see correct message for repeated user
     */
    public function IseeCorrectMessageForRepeatedUser()
    {
        PHPUnit_Framework_Assert::assertTrue('User already exists' == $this->response);
    }
}
