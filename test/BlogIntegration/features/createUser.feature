Feature: Create a User
  In order to create a User
  I need to provide valid email and a valid password

Scenario: New User for system
    # Given User with email 'dan@dan.dan' that is not registerd
    Given email "dan@dan.dan"
    Given password "validpassword123"
    When creating new User
    Then I see correct welcome message

Scenario: Creating 2 users with diferent email
    Given email "dan@dan.dan"
    Given password "validpassword123"
    When creating new User
    Then I see correct welcome message
    Given email "dan2@dan.dan"
    When creating new User
    Then I see correct welcome message

Scenario: Cant create 2 users with same email
    Given email "dan@dan.dan"
    Given password "validpassword123"
    When creating new User
    Then I see correct welcome message
    Given email "dan@dan.dan"
    When creating new User
    Then I see correct message for repeated user