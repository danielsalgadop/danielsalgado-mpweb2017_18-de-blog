<?php

namespace Blog\BlogUnitTest\Aplication\CreateUserCommandHandlerTest;

use Blog\Domain\Repository\UserRepository;
use Blog\Aplication\CommandHandler\CreateUserCommandHandler;
use Blog\Aplication\Command\CreateUserCommand;

use \Exception;
use PHPUnit_Framework_TestCase;

final class CreateUserCommandHandlerTest extends PHPUnit_Framework_TestCase
{
    private $userRepository;
    private $createUserCommand;
    private $email;

    public function setUp()
    {
        $this->email = 'dan@dan.dan';
        $this->userRepository = $this->createMock(UserRepository::class);
        // $this->createUserCommand = $this->createMock(createUserCommand::class);
        $this->createUserCommand = new CreateUserCommand($this->email, 'validpassword123');
    }
    public function tearDown()
    {
        $this->userRepository = null;
    }

    /**
    * @test
    */
    public function createUserCommandHandlerReturnsCorrectMessage()
    {
        $createUserCommandHandler = new CreateUserCommandHandler($this->userRepository);
        $this->assertInstanceOf(CreateUserCommandHandler::class, $createUserCommandHandler);
        $command_response = $createUserCommandHandler->handle($this->createUserCommand);
        $this->assertTrue('User created with email: '.$this->email == $command_response);
        // $this->assertInstanceOf(User::class,$user);
    }
}
