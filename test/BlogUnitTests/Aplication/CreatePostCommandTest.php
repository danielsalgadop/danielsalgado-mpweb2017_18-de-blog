<?php

namespace Blog\BlogUnitTest\Aplication\CreatePostCommandTest;

use Blog\Aplication\Command\CreatePostCommand;
use Blog\Domain\User; // remove this when mocking
use Blog\Domain\Email; // remove this when mocking
use Blog\Domain\Password; // remove this when mocking
use \Exception;
use PHPUnit_Framework_TestCase;
use Faker\Factory;

final class CreatePostCommandTest extends PHPUnit_Framework_TestCase
{
    // private $user;
    // private $password;
    // private $createPostCommand;

    private $s15_characters;
    private $s60_characters;

    public function setUp()
    {

        // $faker = Factory::create();
        $this->s15_characters = 'fake title';
        $this->s60_characters = 'fake body';
        // $this->user = $this->getMockBuilder(User::class)
        //     ->disableOriginalConstructor()
        //     ->getMock();
    }

    /**
    * @test
    */
    public function correctCreation()
    {
        $createPostCommand = new CreatePostCommand($this->s15_characters, $this->s60_characters);
        $this->assertInstanceOf(CreatePostCommand::class, $createPostCommand);
        return $createPostCommand;
    }

    /**
     * @test
     * @depends correctCreation
     */
    public function gettersReturnExpectedValues($createPostCommand)
    {
        // $this->assertInstanceOf(User::class,$createPostCommand->getUser());
        $this->assertEquals($this->s15_characters, $createPostCommand->getTitle());
        $this->assertEquals($this->s60_characters, $createPostCommand->getBody());
    }
}
