<?php

namespace Blog\BlogUnitTest\Aplication\CreateUserCommandTest;

use Blog\Aplication\Command\CreateUserCommand;
use \Exception;
use PHPUnit_Framework_TestCase;

final class CreateUserCommandTest extends PHPUnit_Framework_TestCase
{
    private $email;
    private $password;
    private $createUserCommand;

    public function setUp()
    {
        $this->email = 'dan@dan.dan';
        $this->password = 'validpassword123';
    }

    /**
    * @test
    */
    public function correctCreation()
    {
        $createUserCommand = new CreateUserCommand($this->email, $this->password);
        $this->assertInstanceOf(CreateUserCommand::class, $createUserCommand);
        return $createUserCommand;
    }

    /**
     * @test
     * @depends correctCreation
     */
    public function gettersReturnExpectedValues($createUserCommand)
    {
        $this->assertTrue($this->email == $createUserCommand->getEmail());
        $this->assertTrue($this->password == $createUserCommand->getPassword());
    }
}
