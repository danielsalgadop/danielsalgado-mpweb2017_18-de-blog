<?php

namespace Blog\BlogUnitTest\Aplication\CreatePostCommandHandlerTest;

use Blog\Domain\Repository\PostRepository;
use Blog\Aplication\CommandHandler\CreatePostCommandHandler;
use Blog\Aplication\CommandHandler\CreatexPostCommandHandler;
use Blog\Aplication\Command\CreatePostCommand;

use Blog\BlogUnitTest\Domain\User\FactoryUser;


use \Exception;
use PHPUnit_Framework_TestCase;

final class CreatePostCommandHandlerTest extends PHPUnit_Framework_TestCase
{
    private $postRepository;
    private $createPostCommand;
    private $email;

    public function setUp()
    {
        // pruebas de tener un Factory para Entidades de Dominio
        // $user = FactoryUser::new();

        // print_r($post);
        $this->postRepository = $this->createMock(PostRepository::class);
        // $this->createPostCommand = $this->createMock(createPostCommand::class);
        $this->createPostCommand = new CreatePostCommand('validTitle', 'validBody');
    }
    public function tearDown()
    {
        $this->postRepository = null;
    }

    /**
    * @test
    */
    public function createPostCommandHandlerReturnsCorrectMessage()
    {
        $createPostCommandHandler = new CreatePostCommandHandler($this->postRepository);
        $this->assertInstanceOf(CreatePostCommandHandler::class, $createPostCommandHandler);
        $command_response = $createPostCommandHandler->handle($this->createPostCommand);
        $this->assertEquals('Post created', $command_response);
        // $this->assertInstanceOf(Post::class,$post);
    }
}
