<?php

namespace Blog\BlogUnitTest\Aplication\CreatePostUseCase;

use Blog\Domain\User;
use Blog\Domain\Email;
use Blog\Domain\Password;
use Blog\Domain\Repository\UserRepository;
use Blog\Aplication\Command\CreateUserCommand;
use Blog\Aplication\CommandHandler\CreateUserCommandHandler;


use \Exception;
use PHPUnit_Framework_TestCase;

final class CreatePostUseCaseHandlerTest extends PHPUnit_Framework_TestCase
{
    private $user;
    private $email;
    private $password;
    private $userRepository;
    private $createUserCommand;

    public function setUp()
    {
        $this->user = $this->createMock(User::class);
        $this->email = $this->createMock(Email::class);
        $this->password = $this->createMock(Password::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->createUserCommand = $this->createMock(createUserCommand::class);
    }
    public function tearDown()
    {
        $this->user = null;
        $this->email = null;
        $this->password = null;
        $this->userRepository = null;
        $this->createUserCommand = null;
    }

    /**
    * @test
    */
    public function correctCreation()
    {
        $createUserCommandHandler = new CreateUserCommandHandler($this->userRepository);
    }
}
