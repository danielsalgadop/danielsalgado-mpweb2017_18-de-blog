<?php
namespace Blog\BlogUnitTest\Infraestructure\Post;

use Blog\Infraestructure\Post\FilePostRepository;
use Blog\Domain\Post;
use PHPUnit_Framework_TestCase;
use Faker\Factory;

final class FilePostRepositoryTest extends PHPUnit_Framework_TestCase
{
    const TEST_FOLDER = '/tmp/filePostRepositoryTest';
    const TEST_FILE = 'unit_test_post_repository';
    private $post;
    private $faker;
    private $body;
    private $filePostRepository;

    // TODO test, throws Exception if cant create persistence_path
    protected function setUp()
    {
        $this->faker = Factory::create();
        $this->body = $this->faker->text($maxNbChars = 60);

        $this->removeTestFolderIfExists();
        mkdir(self::TEST_FOLDER);
        $this->filePostRepository = new FilePostRepository(self::TEST_FOLDER, self::TEST_FILE);

        $this->post = $this->createStubPostThatWillReturnThisTitle($this->faker->text($maxNbChars = 5));
        $this->filePostRepository->save($this->post);
    }

    protected function tearDown()
    {
        $this->removeTestFolderIfExists();
    }


    /** @test */
    public function adding1ValidDirectoryForPersistanceData()
    {
        $this->assertDirectoryExists(self::TEST_FOLDER);
        $this->assertDirectoryisReadable(self::TEST_FOLDER);
        $this->assertDirectoryisWritable(self::TEST_FOLDER);
    }

    /**
    * @test
    * @expectedException Blog\Domain\Repository\Exception\PostRepositoryRepeatedPostException
    */
    public function addingPostWithExisitingTitleAndBodyThrowsException()
    {
        $this->filePostRepository->save($this->post);
    }


    /**
    * This test DOES NOT uses stubs. I could not make assertTrue line work. Objects where always different, even I shared the exact same values
    * @test
    * @dataProvider uniqueTitlesProvider
     */
    public function findByTitleReturnsArrayOfPostObjectsThatShareTitle($unique_titles)
    {
        // DUDA Victor. esto da Nesting level too deep - recursive dependency?
        // $this->assertTrue([$this->post] == $this->filePostRepository->findByTitle($this->post->getTitle()));

        $post = new Post($unique_titles, $this->body);
        $post2 = new Post($unique_titles, $this->body.'salt');
        $this->filePostRepository->save($post);
        $this->filePostRepository->save($post2);
        $this->assertEquals([$post,$post2], $this->filePostRepository->findByTitle($unique_titles));
    }

    /** @test */
    public function findByTitleReturnsEmptyArrayOnInexistenTitle()
    {
        $post = new Post('this title is too long to be created by faker', $this->body);
        $this->assertTrue([] === $this->filePostRepository->findByTitle($this->faker->email));
    }

    /**
     * @test
     * Could not use a dataProvider, because each iteraction is a NEW test so size of file was always 1
     */
    public function addingNewPostIncreasesFileIn1Row()
    {
        for ($i=2;$i < 10; $i++) { // tested to $i < 100
            $title = $this->faker->unique()->text($maxNbChars = 25, $minNbChars = 25); // ensure it es unique
            $stub = $this->createStubPostThatWillReturnThisTitle($title, $this->faker->text($maxNbChars = 60));
            $this->filePostRepository->save($stub);
            $this->assertEquals($i, $this->getSizeOfFileRepositoryFile());
        }
    }

    // -------------------------------------------
    // Private methods
    // -------------------------------------------

    private function createStubPostThatWillReturnThisTitle($title)
    {
        $stub = $this->createMock(Post::class);
        $stub->method('getTitle')
                  ->willReturn($title);
        return $stub;
    }

    private function getSizeOfFileRepositoryFile()
    {
        return(count(file(self::TEST_FOLDER. DIRECTORY_SEPARATOR. self::TEST_FILE)));
    }
    public function uniqueTitlesProvider()
    {
        $arr_titles = [];
        $faker = Factory::create();
        for ($i=0; $i<4;$i++) {
            $arr_titles[] = [$faker->unique()->text($maxNbChars = 15)];
        }
        return $arr_titles;
    }

    // TODO - dont post REAL system folder, mock them. Or rmdir if exists  (for consistency between incorrect tests)
    private function removeTestFolderIfExists()
    {
        // TODO - dont post REAL system folder, mock them
        if (file_exists(self::TEST_FOLDER)) {
            system('rm -rf '.self::TEST_FOLDER);
        }
    }
}
