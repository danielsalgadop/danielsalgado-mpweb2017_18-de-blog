<?php
namespace Blog\BlogUnitTest\Infraestructure\User;

use Blog\Infraestructure\User\FileUserRepository;
use Blog\Domain\User;
use Blog\Domain\Email;
use Blog\Domain\Password;
use PHPUnit_Framework_TestCase;
use Faker\Factory;

final class FileUserRepositoryTest extends PHPUnit_Framework_TestCase
{
    const TEST_FOLDER = '/tmp/fileUserRepositoryTest';
    const TEST_FILE = 'unit_test_user_repository';
    private $user;
    private $faker;
    private $valid_password = 'validpassword123';
    private $fileUserRepository;

    // TODO test, throws Exception if cant create persistence_path
    protected function setUp()
    {
        $this->faker = Factory::create();

        // TODO - dont user REAL system folder, mock them. Or rmdir if exists  (for consistency between incorrect tests)
        $this->removeTestFolderIfExists();
        mkdir(self::TEST_FOLDER);
        $this->fileUserRepository = new FileUserRepository(self::TEST_FOLDER, self::TEST_FILE);
        $this->user = $this->createStubWhichGetEmailWillReturnThisValue($this->faker->email);
        $this->fileUserRepository->add($this->user);
    }

    protected function tearDown()
    {
        $this->removeTestFolderIfExists();
    }


    /** @test */
    public function adding1ValidDirectoryForPersistanceData()
    {
        $this->assertDirectoryExists(self::TEST_FOLDER);
        $this->assertDirectoryisReadable(self::TEST_FOLDER);
        $this->assertDirectoryisWritable(self::TEST_FOLDER);
    }

    /**
    * @test
    * @expectedException Blog\Domain\Repository\Exception\UserRepositoryRepeatedUserException
    */
    public function addingUserWithExisitingEmailThrowsException()
    {
        $this->fileUserRepository->add($this->user);
    }


    /**
    * This test DOES NOT uses stubs. I could not make assertTrue line work. Objects where always different, even I shared the exact same email_value
    * @test
     */
    public function findByEmailReturnsCorrectUserObject()
    {
        $email_value = 'dan@dan.dan';
        // $email = $this->createMock(Email::class);
        $email = new Email($email_value);
        $password = new Password('validpassword123');

        $user = new User($email, $password);
        $this->fileUserRepository->add($user);

        $this->assertTrue($user == $this->fileUserRepository->findByEmail($email_value));
    }

    /** @test */
    public function findByEmailReturnsFalseOnInexistentEmail()
    {
        $this->assertTrue(null === $this->fileUserRepository->findByEmail($this->faker->email));
    }

    /**
     * @test
     * Could not use a dataProvider, because each iteraction is a NEW test so size of file was always 1
     */
    public function addingNewUserIncreasesFileIn1Row()
    {
        for ($i=2;$i < 10; $i++) {
            $stub = $this->createStubWhichGetEmailWillReturnThisValue($this->faker->email);
            $this->fileUserRepository->add($stub);
            $this->assertEquals($i, $this->getSizeOfFileRepositoryFile());
        }
    }

    // -------------------------------------------
    // Private methods
    // -------------------------------------------

    private function createStubWhichGetEmailWillReturnThisValue($email)
    {
        $stub = $this->createMock(User::class);
        $stub->method('getEmail')
                  ->willReturn($email);
        return $stub;
    }

    private function getSizeOfFileRepositoryFile()
    {
        return(count(file(self::TEST_FOLDER. DIRECTORY_SEPARATOR. self::TEST_FILE)));
    }

    // TODO - dont post REAL system folder, mock them. Or rmdir if exists  (for consistency between incorrect tests)
    private function removeTestFolderIfExists()
    {
        // TODO - dont post REAL system folder, mock them
        if (file_exists(self::TEST_FOLDER)) {
            system('rm -rf '.self::TEST_FOLDER);
        }
    }
}
