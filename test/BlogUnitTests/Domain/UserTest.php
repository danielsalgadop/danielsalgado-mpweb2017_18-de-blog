<?php
namespace Blog\BlogUnitTest\Domain\User;

use Blog\Domain\User;
use Blog\Domain\Email;
use Blog\Domain\Password;
use Faker\Factory;

use PHPUnit_Framework_TestCase;

final class UserTest extends PHPUnit_Framework_TestCase
{
    private $emailDto;
    private $passwordDto;
    private $email;
    private $password;

    public function setUp()
    {
        $faker = Factory::create();

        $this->email = $faker->email;
        $email_stub = $this->createMock(Email::class);
        $email_stub->method('getEmail')
                            ->willReturn($this->email);
        $this->emailDto = $email_stub;

        $this->password = $faker->password;
        $passwor_stub = $this->createMock(Password::class);
        $passwor_stub->method('getPassword')
                            ->willReturn($this->password);
        $this->passwordDto = $passwor_stub;
    }

    /** @test **/
    public function correctEmailsAndPasswordsReturnUser()
    {
        $user = new User($this->emailDto, $this->passwordDto);
        $this->assertInstanceOf(User::class, $user, 'must be a User object');
        $this->assertObjectHasAttribute('email', $user);
        $this->assertObjectHasAttribute('password', $user);
        $this->assertInstanceOf(Email::class, $user->getEmailDto());
        $this->assertInstanceOf(Password::class, $user->getPasswordDto());
        $this->assertTrue($this->email === $user->getEmail());
        $this->assertTrue($this->password === $user->getPassword());
    }
}


class FactoryUser
{
    public static function new(string $email = 'dan@dan.dan', string $password = 'validpassword123')
    {
        return new User(new Email($email), new Password($password));
    }
}
