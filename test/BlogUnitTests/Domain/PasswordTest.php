<?php
namespace Blog\BlogUnitTest\Domain\Password;

use Blog\Domain\Password;
use Blog\Domain\Exception\InvalidPasswordException;
use PHPUnit_Framework_TestCase;

final class PasswordTest extends PHPUnit_Framework_TestCase
{
    /**
    * @test
    * @dataProvider correctPasswordsProvider
    *  **/
    //TODO rename this method
    public function correctPasswordsReturnPasswordObjectsWithPasswordAttributeAndMethodGetPasswordReturnCorrectPassword($correct_password)
    {
        $password = new Password($correct_password);
        $this->assertInstanceOf(Password::class, $password);
        $this->assertObjectHasAttribute('password', $password);
        $password_returned = $password->getPassword();
        $this->assertTrue($password_returned == $correct_password);
    }

    /**
     * @test
     * @expectedException Blog\Domain\Exception\InvalidPasswordException
     * @dataProvider incorrectPasswordProvider
     * */
    public function incorrectPasswordRaisesInvalidPasswordException($incorrect_password)
    {
        new Password($incorrect_password);
    }

    /**
     * @test
     * */
    public function isEqualReturnsTrueWhen2DtosHaveSameValue()
    {
        $password = 'validpassword123';
        $password_1 = new Password($password);
        $password_2 = new Password($password);
        $this->assertFalse($password_1 === $password_2);
        $this->assertTrue($password_1->isEqual($password_2));
        $this->assertTrue($password_2->isEqual($password_1));
    }

    // DataProviders
    public function correctPasswordsProvider()
    {
        // cant rely on Faker\Factory to generate passwords
        return [
            ['validpassword123'],
            ['validpassword1234'],
            ['validpassword12345'],
        ];
    }

    // DataProviders
    public function incorrectPasswordProvider()
    {
        return [
                    'onlyletters' =>['onlyletters'],['againonlyletters'],
                    'onlynumbers' => ['123'],['1234'],
                    'too small password' => ['1a'],['a1'],
                    'too big password' => ['abcdefghijklm123abcdefghijklm123']
        ];
    }
}
