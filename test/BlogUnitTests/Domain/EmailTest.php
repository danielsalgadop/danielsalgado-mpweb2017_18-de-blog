<?php
namespace Blog\BlogUnitTest\Domain\Email;

use Blog\Domain\Email;
use Blog\Domain\Exception\InvalidEmailException;
use PHPUnit_Framework_TestCase;
use Faker\Factory;

final class EmailTest extends PHPUnit_Framework_TestCase
{
    private $faker;

    public function setUp()
    {
        $this->faker = Factory::create();
    }


    /**
    * @test
    * @dataProvider correctEmailsProvider
    *  **/
    //TODO rename this method
    public function correctEmailsReturnEmailObjectsWithEmailAttributeAndMethodGetEmailReturnCorrectEmail($correct_email)
    {
        // print_r($email);
        $email = new Email($correct_email);
        $this->assertInstanceOf(Email::class, $email);
        $this->assertObjectHasAttribute('email', $email);
        $email_returned = $email->getEmail();
        $this->assertTrue($email_returned == $correct_email);
    }

    /**
     * @test
     * @expectedException Blog\Domain\Exception\InvalidEmailException
     * @dataProvider incorrectEmailProvider
     * */
    public function incorrectEmailRaisesInvalidEmailException($incorrect_email)
    {
        new Email($incorrect_email);
    }

    /**
     * @test
     * */
    public function isEqualReturnsTrueWhen2DtosHaveSameValue()
    {
        $email = 'dan@dan.dan';
        $email_1 = new Email($email);
        $email_2 = new Email($email);
        $this->assertFalse($email_1 === $email_2);
        $this->assertTrue($email_1->isEqual($email_2));
        $this->assertTrue($email_2->isEqual($email_1));
    }

    // DataProviders
    public function correctEmailsProvider()
    {
        $faker = Factory::create(); // TODO move this to setUp
        $arr_correct_emails = [];
        for ($i=0; $i<10;$i++) {
            $arr_correct_emails[] = [$faker->email];
        }
        return $arr_correct_emails;
    }

    public function incorrectEmailProvider()
    {
        return [
                    'no username' =>['@yahoo.com'],['@gmail.com'],
                    'no domain' => ['dan@'],['mary@'],
                    'no at' => ['danyahoo.com'],['danXyahoo.com'],
                    'no top level domain' => ['dan@yahoo'], ['dan@gmail']
        ];
    }
}
