<?php
namespace Blog\BlogUnitTest\Domain\User;

use Blog\Domain\Post;
use Blog\Domain\Exception\InvalidPostException;
use Faker\Factory;
use PHPUnit_Framework_TestCase;

final class PostTest extends PHPUnit_Framework_TestCase
{
    private $s15_characters;
    private $s60_characters;
    private $s201_characters;
    private $post;

    public function setUp()
    {
        // TODO make dataProviders for correct Values
        $faker = Factory::create();
        // I am not using dataProviders because some tests need two types of data
        $this->s15_characters = $faker->text($maxNbChars = 15);
        $this->s60_characters = $faker->text($maxNbChars = 60);
        $this->s201_characters = $faker->text($maxNbChars = 201);
    }


    /**
    * @test
    *  */
    public function correctSizeForBodyAndTitleCreatesPost()
    {
        $this->post = new Post($this->s15_characters, $this->s15_characters);
        $this->assertInstanceOf(Post::class, $this->post, 'must be a Post class');
        $this->postHasThisTitle($this->s15_characters);
        $this->postHasThisBody($this->s15_characters);
    }
    /**
    * @test
    * TODO think how to test private property publish
    * */
    public function correctSizeForBodyTitleAndPublishFlagCreatesPostWithParameterTrue()
    {
        $this->post = new Post($this->s15_characters, $this->s15_characters, true);
        $this->assertInstanceOf(Post::class, $this->post, 'Post was not created');
    }

    /**
     * @test
     * @expectedException Blog\Domain\Exception\InvalidPostException
     *  */
    public function newPostwithIncorrectTitleRaisesException()
    {
        new Post($this->s201_characters, $this->s15_characters);
    }

    # TODO
    public function setPublishMethodChangesPublishObjectProperty()
    {
    }
    /**
    * @test
    * @dataProvider strangeValuesForTitleBodyAndPublshParamsProvider
    * @expectedException Blog\Domain\Exception\InvalidPostException
    *  */
    public function strangeParamsForPostRaisesException($title, $body, $publish)
    {
        $post = new Post($title, $body, $publish);
    }

    // TODO make dataProviders for correct Values
    // dataProviders
    public function strangeValuesForTitleBodyAndPublshParamsProvider()
    {
        return[
                ['','',''],
                [0,0,0],
                [$this->s15_characters,0,0],
                [$this->s15_characters,$this->s15_characters,0],
                [$this->s15_characters,$this->s15_characters,'WRONG_VALUE_BECAUSE_NOT_BOOLEAN'],
                ['',$this->s15_characters,true],
                [$this->s15_characters,'',true],
            ];
    }

    // private Functions
    private function postHasThisTitle($title)
    {
        $this->assertTrue($title == $this->post->getTitle());
    }
    private function postHasThisBody($title)
    {
        $this->assertTrue($title == $this->post->getTitle());
    }
}
