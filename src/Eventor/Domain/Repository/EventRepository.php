<?php

namespace Eventor\Domain\Repository;

use Eventor\Domain\Event;

interface EventRepository
{
    public function set(Event $Event);
    public function get():Event;
}
