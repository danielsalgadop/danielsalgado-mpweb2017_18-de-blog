<?php
namespace Eventor\Domain;

use \Exception;

class Event
{
    private $event_line;
    const EVENT_LINE_SEPARATOR = 'X';
    public function __construct($arr_info)
    {
        $this->event_line = implode(self::EVENT_LINE_SEPARATOR, $arr_info);
    }
}
