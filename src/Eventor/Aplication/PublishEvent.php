<?php

namespace Eventor\Application;

use Eventor\Domain\Event;
use Eventor\Domain\Repository\EventRepository;

class PublishEvent
{
    public function __construct(Event $event, EventRepository $eventRepository)
    {
        $eventRepository->set($event);
    }
}
