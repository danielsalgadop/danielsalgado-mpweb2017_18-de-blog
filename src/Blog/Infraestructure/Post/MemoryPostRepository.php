<?php
namespace Blog\Infraestructure\Post;

use Blog\Domain\Repository\PostRepository;
use Blog\Domain\Post;
use Exception;

class MemoryPostRepository implements PostRepository
{
    private $arr = [];
    public function add(Post $user)
    {
        if ($this->findByEmail($user->getEmail())) {
            throw new Exception("Post already exists", 1);
        }
        $this->arr[] = $user;
    }
    public function findByEmail(string $email) : ? Post
    {
        if (empty($this->arr)) {
            return null;
        }
        foreach ($this->arr as $user) {
            if ($user->getEmail() ==  $email) {
                return $user;
            }
        }
        return null;
    }
    public function __toString() : string
    {
        $str = '';
        if (count($this->arr) == 0) {
            return $str;
        }
        for ($i=0;$i<count($this->arr); $i++) {
            $user = $this->arr[$i];
            $str .= $i.' '.$user->getEmail().' '.$user->getPassword()."\n";
        }
        return $str;
    }
}
