<?php

namespace Blog\Infraestructure\Post;

use Blog\Domain\Post;
use Blog\Domain\Repository\PostRepository;
use Blog\Domain\Repository\Exception\PostRepositoryRepeatedPostException;

class FilePostRepository implements PostRepository
{
    // public function saveAndPublish(Post $post);
    // public function saveButDontPublish(Post $post);
    // public function getPostById(int $id);
    // public function getPostByTitle(string $title);

    public $persistence_path;
    public function __construct($path = "/tmp", $filename = "file_post_repository")
    {
        $this->persistence_path = $path.'/'.$filename;
        $fh = fopen($this->persistence_path, 'w');
        if (!$fh) {
            throw new Exception("Cant create persistence path", 1);
        }
    }

    public function save(Post $post) : void
    {
        if ($this->exists($post)) {
            throw new PostRepositoryRepeatedPostException("Post already exists", 1);
        }
        file_put_contents($this->persistence_path, serialize($post)."\n", FILE_APPEND);
    }

    // TODO esto va a devolver un array
    public function findByTitle(string $title) : ? array
    {
        $arr_return = [];
        $arr_persistence_path = file($this->persistence_path);
        foreach ($arr_persistence_path as $serialized_post) {
            $post = unserialize($serialized_post);
            if ($post->getTitle() == $title) {
                $arr_return[] = $post;
            }
        }
        return $arr_return;
    }
    // TODO esto va a devolver un array
    public function findByBody(string $body) : ?Post
    {
        $arr_persistence_path = file($this->persistence_path);
        foreach ($arr_persistence_path as $serialized_post) {
            $post = unserialize($serialized_post);
            if ($post->getBody() == $body) {
                return $post;
            }
        }
        return null;
    }
    public function exists(Post $post) : bool
    {
        $arr_persistence_path = file($this->persistence_path);

        foreach ($arr_persistence_path as $serialized_post) {
            $repo_post = unserialize($serialized_post);
            if ($repo_post->getTitle() == $post->getTitle() && $repo_post->getBody() == $post->getBody()) {
                return true;
            }
        }
        return false;
    }

    public function __toString() : string
    {
        $str = '';
        foreach (file($this->persistence_path) as $serialized_post) {
            $post = unserialize($serialized_post);
            $str .= $post->getEmail(). ' '. $post->getPassword()."\n";
        }
        return $string;
    }
}
