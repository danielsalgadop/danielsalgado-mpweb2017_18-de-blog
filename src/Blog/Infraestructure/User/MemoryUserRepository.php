<?php
namespace Blog\Infraestructure\User;

use Blog\Domain\Repository\UserRepository;
use Blog\Domain\User;
use Exception;

class MemoryUserRepository implements UserRepository
{
    private $arr = [];
    public function add(User $user)
    {
        if ($this->findByEmail($user->getEmail())) {
            throw new Exception("User already exists", 1);
        }
        $this->arr[] = $user;
    }
    public function findByEmail(string $email) : ? User
    {
        if (empty($this->arr)) {
            return null;
        }
        foreach ($this->arr as $user) {
            if ($user->getEmail() ==  $email) {
                return $user;
            }
        }
        return null;
    }
    public function __toString() : string
    {
        $str = '';
        if (count($this->arr) == 0) {
            return $str;
        }
        for ($i=0;$i<count($this->arr); $i++) {
            $user = $this->arr[$i];
            $str .= $i.' '.$user->getEmail().' '.$user->getPassword()."\n";
        }
        return $str;
    }
}
