<?php

namespace Blog\Infraestructure\User;

use Blog\Domain\User;
use Blog\Domain\Repository\UserRepository;
use Blog\Domain\Repository\Exception\UserRepositoryRepeatedUserException;

class FileUserRepository implements UserRepository
{
    public $persistence_path;
    public function __construct($path = "/tmp", $filename = "file_user_repository")
    {
        $this->persistence_path = $path.'/'.$filename;
        $fh = fopen($this->persistence_path, 'w');
        if (!$fh) {
            // DUDA Victor: this Exception IS-A Infraestructure Exception, not Domain or Aplication
            throw new Exception("Cant create persistence path", 1);
        }
    }

    public function add(User $user) : void
    {
        if ($this->findByEmail($user->getEmail())) {
            throw new UserRepositoryRepeatedUserException("User already exists", 1);
        }
        file_put_contents($this->persistence_path, serialize($user)."\n", FILE_APPEND);
    }

    public function findByEmail(string $email) : ?User
    {
        $arr_persistence_path = file($this->persistence_path);
        foreach ($arr_persistence_path as $serialized_user) {
            $user = unserialize($serialized_user);
            if ($user->getEmail() == $email) {
                return $user;
            }
        }
        return null;
    }
    public function __toString() : string
    {
        $str = '';
        foreach (file($this->persistence_path) as $serialized_user) {
            $user = unserialize($serialized_user);
            $str .= $user->getEmail(). ' '. $user->getPassword()."\n";
        }
        return $str;
    }
}
