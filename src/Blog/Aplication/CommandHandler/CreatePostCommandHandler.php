<?php

namespace Blog\Aplication\CommandHandler;

use Blog\Aplication\CommandHandler\HandlerInterface;
use Blog\Domain\Repository\PostRepository;
use Blog\Domain\Post;
use \Exception;
use Blog\Domain\Exception\InvalidEmailException;
use Blog\Domain\Repository\Exception\PostRepositoryRepeatedPostException;

class CreatePostCommandHandler implements HandlerInterface
{
    private $postRepository;
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function handle($command)
    {
        try {
            // $user = $command->getUser();
            $post = new Post($command->getTitle(), $command->getBody());
            $this->postRepository->save($post);
            $this->postRepository->save($post);
        } catch (InvalidPostException $e) {
            return $e->getMessage();
        } catch (PostRepositoryRepeatedPostException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return "Post created";
    }
}
