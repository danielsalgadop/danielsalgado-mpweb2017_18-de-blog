<?php

namespace Blog\Aplication\CommandHandler;

use Blog\Aplication\CommandHandler\HandlerInterface;
use Blog\Domain\Repository\UserRepository;
use Blog\Domain\User;
use Blog\Domain\Email;
use Blog\Domain\Password;
use \Exception;
use Blog\Domain\Exception\InvalidEmailException;

// use Blog\Domain\Repository\Exception\UserRepositoryRepeatedUserException;


class CreateUserCommandHandler implements HandlerInterface
{
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle($command)
    {
        try {
            $user = new User(new Email($command->getEmail()), new Password($command->getPassword()));
            if ($this->userRepository->findByEmail($command->getEmail())) {
                return "User already exists";
                // throw new UserRepositoryRepeatedUserException();
            }
            $this->userRepository->add($user);
        } catch (InvalidEmailException $e) {
            return $e->getMessage();
        } catch (InvalidPasswordException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return "User created with email: ".$user->getEmail();
    }
}
