<?php
namespace Blog\Aplication\CommandHandler;

interface HandlerInterface
{
    public function handle($comand);
}
