<?php

namespace Blog\Aplication\Command;

use Blog\Domain\User;

class CreatePostCommand
{
    // private $user;
    private $title;
    private $body;

    public function __construct(string $title, string $body)
    {
        // $this->user = $user;
        $this->title = $title;
        $this->body = $body;
    }
    // public function create(Post $post, User $user){

    // }

    // public function getUser()
    // {
    //     return $this->user;
    // }

    public function getTitle()
    {
        return $this->title;
    }

    public function getBody()
    {
        return $this->body;
    }
}
