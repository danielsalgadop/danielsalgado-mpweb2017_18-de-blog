<?php

namespace Blog\Domain;

use Blog\Domain\Exception\InvalidEmailException;

class Email
{
    private $email;
    public function __construct($email)
    {
        $this->isValidEmailOrThrowException($email);
        $this->email = $email;
    }

    private function isValidEmailOrThrowException($email) : bool
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException("Incorrect email", 1);
        }
        return true;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function isEqual(Email $other) : bool
    {
        return $this->getEmail() === $other->getEmail();
    }
}
