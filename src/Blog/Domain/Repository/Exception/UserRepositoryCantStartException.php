<?php

namespace Blog\Domain\Repository\Exception;

use \Exception;

class UserRepositoryCantStartException extends Exception
{
    public function __construct($message = '', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
