<?php

namespace Blog\Domain\Repository;

use Blog\Domain\User;

interface UserRepository
{
    public function add(User $user);
    public function findByEmail(string $email) : ? User;
    public function __toString(): string;
}
