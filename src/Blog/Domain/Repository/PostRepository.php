<?php

namespace Blog\Domain\Repository;

use Blog\Domain\Post;

interface PostRepository
{
    // public function create(Post $post);
    // public function saveAndPublish(Post $post);
    // public function saveButDontPublish(Post $post);
    public function save(Post $post);
    // public function publish(EventQueueRepository $eventRepository);
    public function findByTitle(string $title);
    public function findByBody(string $body);
    public function exists(Post $post) : bool;
}
