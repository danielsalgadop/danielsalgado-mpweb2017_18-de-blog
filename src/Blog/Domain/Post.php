<?php
namespace Blog\Domain;

use Blog\Domain\Exception\InvalidPostException;

/*
Cada Post tendrá un título y un cuerpo. El título no puede superar los 50 caracteres y el cuerpo no puede superar los 2000 caracteres.

 Para crear el Post existen dos opciones:
        solo guardarlo y no publicarlo en el blog
        guardarlo y publicarlo en el blog. Si el Post ya existe no se volverá a guardar ni se publicará.
*/


class Post
{
    const MAX_SIZE_BODY = 2000;
    const MAX_SIZE_TITLE = 50;
    private $title;
    private $body;
    private $publish;

    public function __construct($title, $body, $publish = false)
    {
        $this->isValidTitle($title);
        $this->isValidBody($body);
        $this->isValidPublish($publish);

        $this->title = $title;
        $this->body = $body;
        $this->publish = $publish;

        // $this->title = filter_var($title, FILTER_SANITIZE_STRING);
        // $this->body = filter_var($body, FILTER_SANITIZE_STRING);
    }

    private function isValidTitle($title) : bool
    {
        if (strlen($title) > self::MAX_SIZE_TITLE || $title === '') {
            throw new InvalidPostException("Incorrect title", 1);
        }
        return true;
    }

    private function isValidBody($body) : bool
    {
        if (strlen($body) > self::MAX_SIZE_BODY || $body === '') {
            throw new InvalidPostException("Incorrect body", 1);
        }
        return true;
        // return Validator::maxString($this->body, (int) 'MAX_SIZE_BODY');
    }

    private function isValidPublish($publish) : bool
    {
        if (gettype($publish) !== 'boolean') {
            throw new InvalidPostException("Incorrect publish value", 1);
        }
        return true;
    }

    public function publish() : void
    {
        $this->publish = true;
    }

    public function unPublish() : void
    {
        $this->publish = false;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function getBody() : string
    {
        return $this->body;
    }
}
