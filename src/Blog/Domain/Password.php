<?php

namespace Blog\Domain;

use Blog\Domain\Exception\InvalidPasswordException;

class Password
{
    const MAX_SIZE_PASSWORD = 28;
    const MIN_SIZE_PASSWORD = 3;

    private $password;
    public function __construct(string $password)
    {
        $this->isValidPasswordOrThrowException($password);
        $this->password = $password;
    }

    private function isValidPasswordOrThrowException($password) : bool
    {
        if (strlen($password) > self::MAX_SIZE_PASSWORD) {
            throw new InvalidPasswordException("Password too Long".$password, 1);
        }
        if (strlen($password) < self::MIN_SIZE_PASSWORD) {
            throw new InvalidPasswordException("Password too Short ".$password, 1);
        }
        if (preg_match('/[a-zA-Z]+/', $password) === 0) {
            throw new InvalidPasswordException("Password must have at least 1 letter", 1);
        }
        if (preg_match('/[0-9]+/', $password) === 0) {
            throw new InvalidPasswordException("Password must have at least 1 number", 1);
        }
        return true;
    }

    public function getPassword() : string
    {
        return $this->password;
    }

    public function isEqual(Password $other) : bool
    {
        return $this->getPassword() === $other->getPassword();
    }
}
