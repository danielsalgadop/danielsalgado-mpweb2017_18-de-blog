<?php
namespace Blog\Domain;

use Blog\Domain\Exception\InvalidPasswordException;
use \Exception;

// Cada Post lo publicará un usuario, que deberá tener las siguientes características:
//         Cada usuario tendrá un Email y un Password.


/**
*
*/
class User
{
    private $email;
    private $password;

    public function __construct(Email $email, Password $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function getEmail() : string
    {
        return $this->getEmailDto()->getEmail();
    }

    public function getPassword() : string
    {
        return $this->getPasswordDto()->getPassword();
    }

    public function getEmailDto() : Email
    {
        return $this->email;
    }

    public function getPasswordDto() : Password
    {
        return $this->password;
    }
}
