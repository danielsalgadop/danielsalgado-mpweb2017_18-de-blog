<?php

namespace Blog\Domain\Exception;

use \Exception;

class InvalidPostException extends Exception
{
    public function __construct($message = '', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
