Hola Victor,

He tomado la decision de usar persistencia en fichero

Se que estoy usando filesystem en test unitarios, y eso no debería ser. Pero es que he intentado usar vfSystem y no me salia. Creo que lo que he aprendido de tests compensa ese fallo.


Según he ido avanzando en tests y al ver comportamiento que se repetía he ido refactorizando, pero lo que el tiempo me ha permitido.

Como me veia creando Entidades y Mocks de estas Entidades repetidamente a lo largo de código he acabado haciendo factorias para compartir código entre tests (me pregunto si se podría usar traits para esto).

En vez de considerar Event como algo parte de los Post, lo he considerado como su propio servicio independiente

Los tests de Behat cuando usan phpunit me parece que solapan a phpunit. Sé que behat usa Repositorios en memoria y phpunit los mockea, pero aún así testean de una manera muy similar el mismo código. Un ejemplo :
- phpunit en  createUserCommandHandlerReturnsUserObject
- behat scenario con creacion de User